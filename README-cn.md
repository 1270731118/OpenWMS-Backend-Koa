## OpenWMS-Backend-Koa

### 项目介绍

基于 Koa 的服务端。

### 用法

    git clone https://gitee.com/mumu-osc/OpenWMS-Backend-Koa.git
    cd OpenWMS-Backend-Koa
    cnpm install
    cnpm start

打开你的浏览器访问http://localhost:3000
