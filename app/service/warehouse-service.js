import BaseService from "./BaseService";
import dbHelper from "../utils/db-helper";
class WarehouseService extends BaseService{
  emptyData = {
    name: "",
    label: "",
    value: "",
    cellphone: "",
    telephone: "",
    addr: "",
    remark: ""
  }

  getById(id) {
    return dbHelper.getById(`select * from warehouse where id=@id`, {id});
  }
  list(params) {
    const {
      pageNumber,
      pageSize,
      name,
      label,
      value,
      cellphone,
      telephone,
      addr,
      remark
    } = Object.assign({}, this.emptyData, this.pagination, params);

    const whereSegment = `where name like @name and label like @label
      and cellphone like @cellphone and value like @value 
      and telephone like @telephone and addr like @addr and remark like @remark`;

    const sqlParams = {
      name: `%${name}%`,
      label: `%${label}%`,
      value: `%${value}%`,
      cellphone: `%${cellphone}%`,
      telephone: `%${telephone}%`,
      addr: `%${addr}%`,
      remark: `%${remark}%`
    };

    //total
    const total = dbHelper.count(`select count(*) from warehouse ${whereSegment}`, sqlParams);
    //rows
    const rows = dbHelper.queryRows(`select 
      id, name,
      label,value,
      cellphone,telephone,addr,remark
      from warehouse ${whereSegment} limit @offset, @limit`,
    Object.assign(sqlParams, {
      offset: (pageNumber - 1) * pageSize,
      limit: pageSize
    }));
    return {
      rows,
      total
    };
  }

  add(params) {
    return dbHelper.insert("insert into warehouse values(null, @name, @label, @value,@cellphone, @telephone, @addr,@remark)", Object.assign({}, this.emptyData, params));
  }

  update(id, params) {
    return dbHelper.update("update warehouse set name=@name, label=@label, value=@value, cellphone=@cellphone, telephone=@telephone, addr=@addr, remark=@remark where id =@id", Object.assign({
      id
    }, this.emptyData, params));
  }

  delete(id) {
    return dbHelper.delete("delete from warehouse where id = @id", {
      id
    });
  }
}
export default new WarehouseService;
