import Router from "koa-router";
import categoryService from "../service/category-service";
import koaBody from "koa-body";
const router = new Router({
  prefix: "/categories"
});

router.get("/:id", async (ctx, next) => {
  const category = categoryService.getById(ctx.params.id);
  ctx.body = category;
  next();
});

router.get("/", async (ctx, next) => {
  const categories = categoryService.list(ctx.query);
  ctx.body = categories;
  next();
});

router.post("/", koaBody(), async (ctx, next) => {
  const res = categoryService.add(ctx.request.body);
  ctx.body = res;
  next();
});

router.put("/:id", koaBody(), async (ctx, next) => {
  const id = ctx.params.id;
  const res = categoryService.update(id, ctx.request.body);
  ctx.body = res;
  next();
});

router.delete("/:id", async (ctx, next) => {
  const id = ctx.params.id;
  const res = categoryService.delete(id);
  ctx.body = res;
  next();
});

export default router;
